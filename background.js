const addUserFilterInterface = (tab,cookies) => {

  //make sure the current website is running the core mastodon client
  const initialState = document.getElementById("initial-state");
  if (initialState && JSON.parse(initialState.innerText)?.meta?.source_url!=="https://github.com/mastodon/mastodon")
    return;

  //make sure the user is logged in 
  if(!cookies.find(cookie => cookie.name==="_session_id"))
    return;
  
  //make sure page is on timeline view
  if (window.location.pathname!=="/home") 
    return;

  const buttonID = "user-filter-button";
  const sectionID = "user-filter-section";

  if (!document.getElementById(buttonID)) {
    buttonsWrapper = document.getElementsByClassName('column-header__buttons')[0];
    button = document.createElement("button");
    button.id=buttonID;
    button.classList.add("column-header__button");
    button.innerHTML = `<i class="icon-with-badge"><i class="fa fa-at"></i></i>`;

    headerWrapper = document.getElementsByClassName("column-header__wrapper")[0];
    section = document.createElement("div");
    section.id = sectionID;
    section.classList.add("column-header__collapsible");
    section.classList.add("collapsed");
    section.innerHTML =`<div class="column-header__collapsible-inner"><div class="column-header__collapsible__extra"><div id="user-filters">My filters go here!</div></div></div>`;

    headerWrapper.appendChild(section);
    buttonsWrapper.appendChild(button);

    const filterDiv = document.getElementById("user-filters");

    const filterRow = (username) => {
      row = document.createElement("tr");
      row.key = `${username}`
      checkbox = document.createElement("input");
      checkbox.type="checkbox";
      checkbox.data = document.userFilterData.summary[username];
      checkbox.checked = checkbox.data.checked;
      checkbox.addEventListener("change",(e) => {
        for (post of e.target.data.posts) {
          if (e.target.checked) {
            post.style.display = "";
            e.target.data.checked = true;
          } else {
            post.style.display = "none";
            e.target.data.checked = false;
          }
        }
      })
      row.innerHTML = `<td class="username">${username}</td> <td class="postCount" align="center">${checkbox.data.count}</td>`;
      checkBoxColumn = document.createElement("td");
      checkBoxColumn.align = "center";
      checkBoxColumn.appendChild(checkbox);
      row.appendChild(checkBoxColumn);
      return row;
    }

    const updateFilterView = () => {
      if (document.userFilterData.summary) {
        summary = document.userFilterData.summary;
        usernames = Object.entries(summary).map(([username,data])=>username).sort(
          (a, b) => a.toLowerCase() < b.toLowerCase() ? -1 : a.toLowerCase() > b.toLowerCase() ? 1 : 0
        );
        filterDiv.innerHTML = "";
        filterTable = document.createElement("table");
        filterTable.width = "100%";
        filterTable.innerHTML = `<tr><th scope="col"></th><th scope="col">Post Count</th><th scope="col">Show</th></tr>`;
        for (username of usernames) {
          if(username !== "undefined"){
            filterTable.appendChild(filterRow(username));
          }
        }
        filterDiv.appendChild(filterTable);
      }
    } 

    const addFilters = (articles) => {
      let timelineSummary = {};
  
      if (!document.userFilterData)
        document.userFilterData = {};

      if (document.userFilterData.summary) {
        timelineSummary = document.userFilterData.summary;
        postIDs = document.userFilterData.postIDs;
      } else {
        timelineSummary = document.userFilterData.summary = {};
        postIDs = document.userFilterData.postIDs = [];
      }
  
      for (article of articles) {
        if (postIDs.includes(article.dataset.id))
          continue;
        else
          postIDs.push(article.dataset.id);
        user = article.getElementsByClassName("display-name__account")[0]?.innerText;
        userData = timelineSummary[user];
        if (userData) {
          timelineSummary[user].posts.push(article);
          timelineSummary[user].count++;
        } else {
          timelineSummary[user] = { posts:[article], count:1, checked: true };
        }
      }
      
      updateFilterView();
    }

    const articles = document.getElementsByTagName("article");
    addFilters(articles);

    var observer = new MutationObserver(function(mutations) {
        const insertedNodes = [];
        mutations.forEach((mutation) => {
        for (var i = 0; i < mutation.addedNodes.length; i++)
            insertedNodes.push(mutation.addedNodes[i]);
        })
        addFilters(insertedNodes);
    });
    const postFeed = document.querySelector("[role=feed]");
    observer.observe(postFeed, { childList: true });
    

    button.addEventListener("click", (e) => {
      e.preventDefault();
      buttons = document.getElementsByClassName("column-header__button");
      button = document.getElementById(buttonID);
      section = document.getElementById(sectionID);
      e.stopPropagation();
      if(button.classList.contains("active")) {
        button.classList.remove("active");
        section.classList.add("collapsed");
      } else {
        button.classList.add("active");
        section.classList.remove("collapsed");
      }
    },true);

  }

}

function navigationCompletedHandler(tab) {

  if(tab.frameType!=="outermost_frame")
    return;

  const urlsToIgnore=/^(chrome|about):.*$/
  if(urlsToIgnore.test(tab.url))
    return;

  try {
    const urlInfo = new URL(tab.url);
    chrome.cookies.getAll({domain:urlInfo.hostname},(cookies)=> {
      // TODO: inject CSS here using chrome.scripting.insertCSS
      chrome.scripting.executeScript(
        {
          target: { tabId: tab.tabId },
          func: addUserFilterInterface,
          args: [tab, cookies]
        }
      )
    });
  } catch(e) {
    console.log(errors);
  }

}

if (chrome.webNavigation) {
  chrome.webNavigation.onCompleted.addListener(navigationCompletedHandler);
  chrome.webNavigation.onHistoryStateUpdated.addListener(navigationCompletedHandler);
}
