# Mastodon Chrome Extension

## Status
work in progress

## Features
Current features
- filter out specific users from your local timeline

Note: this chrome extension is designed to work with the [core Mastodon client](https://github.com/mastodon/mastodon) only

## Demo
[Click here to watch a demo](https://gitlab.com/mastostools/mastodonPlus/-/blob/main/demo.mp4)

## Installation instructions

### 1. Download the extension to your local computer

- **Download:**  
  The extension can be downloaded by clicking on the download icon at the top of this project page.  
  Unzip the file, once it has been downloaded.

- **Clone:**  
  Alternatively, you can clone the project using git

```
git clone https://gitlab.com/mastostools/mastodonPlus.git
```

### 2. Install the extension

**In Google Chrome:**

  1. Type `chrome://extensions/` into the address bar.
  2. Enable *Developer mode* (switch is at the top right of the page)
  3. Click on *Load Unpacked*
  4. Locate the download folder and select it


## Additional Feature Wish List (TODO)
- Store settings in localStorage
- Finer grained filters (per user)
  - boost
  - reply to self
  - reply to someone else 
  - tag or keyword
- Global filter settings
  - Combine all boost posts into a single post
  - Combine all threads to a single post
  - Combine posts that share the same link
  - Combine posts that share the same hashtag
  - Filter out posts that mention a certain user